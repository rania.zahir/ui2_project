package com.example.groeiproject

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.groeiproject.model.Functions
import com.example.groeiproject.model.Youtuber
import com.example.groeiproject.ui.theme.GroeiProjectTheme
import com.example.groeiproject.ui.screen.*
import com.example.groeiproject.utils.SharedPreferencesHelper

class MainActivity : ComponentActivity() {
    private val functions = Functions()
    private val youtubers = mutableStateListOf<Youtuber>().apply {
        addAll(functions.getYoutubers())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Create or retrieve the LanguageViewModel
        val languageViewModel: LanguageViewModel by viewModels()

        // Wrap the content in a Composable function
        setContent {
            MainContent(youtubers, languageViewModel)
        }
    }
}

@Composable
fun MainContent(youtubers: SnapshotStateList<Youtuber>, languageViewModel: LanguageViewModel) {
    val sharedPreferencesHelper = SharedPreferencesHelper(LocalContext.current)

    // Observe the theme preference
    val themePreference by sharedPreferencesHelper.themePreferenceFlow.collectAsState(initial = "light")
    val isDarkTheme = themePreference == "dark"

    GroeiProjectTheme(darkTheme = isDarkTheme) {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            val navController = rememberNavController()
            NavHost(navController = navController, startDestination = "list") {
                composable("list") {
                    YoutuberListScreen(youtubers = youtubers, navController = navController)
                }
                composable(
                    "detail/{youtuberId}",
                    arguments = listOf(navArgument("youtuberId") { type = NavType.IntType })
                ) { backStackEntry ->
                    val youtuberId = backStackEntry.arguments?.getInt("youtuberId")
                    val currentIndex = youtubers.indexOfFirst { it.id == youtuberId }
                    val youtuber = youtubers.find { it.id == youtuberId }
                    youtuber?.let {
                        DetailScreen(
                            youtuber = it,
                            youtubers = youtubers,
                            currentIndex = currentIndex,
                            onExperienceUpdated = { id, newExperience ->
                                val index = youtubers.indexOfFirst { it.id == id }
                                if (index != -1) {
                                    youtubers[index] = youtubers[index].copy(yearsExperience = newExperience)
                                }
                            },
                            onDeleteYoutuber = { id ->
                                val index = youtubers.indexOfFirst { it.id == id }
                                if (index != -1) {
                                    youtubers.removeAt(index)
                                    navController.popBackStack() // Navigate back to the list screen
                                }
                            },
                            onNavigateToYoutuber = { id ->
                                navController.navigate("detail/$id")
                            },
                            onNavigateBack = {
                                navController.popBackStack()
                            },
                            onCreateYoutuber = {
                                navController.navigate("create")
                            },
                            onNavigateToSettings = {
                                navController.navigate("settings")
                            },
                            languageViewModel = languageViewModel
                        )
                    }
                }
                composable("create") {
                    CreateYoutuberScreen(navController = navController, addYoutuber = { newYoutuber ->
                        youtubers.add(newYoutuber)
                    })
                }
                composable("settings") {
                    SettingsScreen(sharedPreferencesHelper = sharedPreferencesHelper)
                }
            }
        }
    }
}
