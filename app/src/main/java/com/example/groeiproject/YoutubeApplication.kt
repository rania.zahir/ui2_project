package com.example.groeiproject

import dagger.hilt.android.HiltAndroidApp;
import android.app.Application;

@HiltAndroidApp
class YoutubeApplication : Application()