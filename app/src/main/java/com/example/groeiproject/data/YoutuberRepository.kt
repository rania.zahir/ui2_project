package com.example.groeiproject.data

import com.example.groeiproject.model.YoutubeChannel
import com.example.groeiproject.model.Youtuber
import com.example.groeiproject.network.YoutubeApiService
import retrofit2.HttpException
import java.io.IOException


interface YoutubeRepository {
    suspend fun getChannels(): List<YoutubeChannel>
    suspend fun getYoutuberByYoutubechannelId(youtubechannelId: Int): List<Youtuber>?

    suspend fun getYoutuber(): List<Youtuber>
    suspend fun addYoutuber(youtuber: Youtuber): Youtuber?
    suspend fun deleteYoutuber(id: String): Boolean
    suspend fun updateYoutuber(youtuber: Youtuber): Youtuber?
    suspend fun getYoutubechannelById(id: Int): YoutubeChannel?
    suspend fun getYoutuberById(id: Int): Youtuber?

}

class NetworkYoutubeRepository(val youtubeApiService: YoutubeApiService) : YoutubeRepository {

    override suspend fun getChannels(): List<YoutubeChannel> {
        try {
            val response = youtubeApiService.getYoutubechannelList()

            if (response.isSuccessful) {
                return response.body() ?: emptyList()
            } else {
                // Handle the error case
                throw HttpException(response)
            }
        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun getYoutuber(): List<Youtuber> {
        try {
            val response = youtubeApiService.getYoutuberList()

            if (response.isSuccessful) {
                return response.body() ?: emptyList()
            } else {
                // Handle the error case
                throw HttpException(response)
            }
        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun addYoutuber(youtuber: Youtuber): Youtuber? {
        try {
            val response = youtubeApiService.addYoutuber(youtuber)
            if (response.isSuccessful) {
                return response.body()
            }
            // Handle the error case
            throw HttpException(response)

        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun deleteYoutuber(id: String): Boolean {
        try {
            val response = youtubeApiService.deleteYoutuber(id)
            return response.isSuccessful
        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        }
    }


    override suspend fun updateYoutuber(youtuber: Youtuber): Youtuber? {
        try {
            val response = youtubeApiService.updateYoutuber(youtuber.id, youtuber)
            if (response.isSuccessful) {
                return response.body()
            }
            // Handle the error case
            throw HttpException(response)

        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun getYoutubechannelById(id: Int): YoutubeChannel? {
        try {
            val response = youtubeApiService.getYoutubechannelById(id)
            if (response.isSuccessful) {
                return response.body()
            }
            // Handle the error case
            throw HttpException(response)

        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun getYoutuberByYoutubechannelId(youtubechannelId: Int): List<Youtuber>? {
        val response = youtubeApiService.getYoutuberList()
        if (response.isSuccessful) {
            return response.body()?.filter { it.id == youtubechannelId }
        }
        return null
    }
    override suspend fun getYoutuberById(id: Int): Youtuber? {
        try {
            val response = youtubeApiService.getYoutuberById(id)
            if (response.isSuccessful) {
                return response.body()
            }
            // Handle the error case
            throw HttpException(response)

        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }


}