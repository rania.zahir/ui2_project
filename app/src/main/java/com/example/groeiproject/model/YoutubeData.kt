package com.example.groeiproject.model
import com.example.groeiproject.R

import androidx.annotation.DrawableRes
import kotlinx.serialization.Contextual
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Serializable
data class YoutubeChannel(
    val id: Int,
    val name: String,
    val category: String,
    val subscribers: Int,
    @DrawableRes val profilePicture: Int,
    val ownerId: Int
)
@Serializer(forClass = LocalDate::class)
object LocalDateSerializer : KSerializer<LocalDate> {
    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("LocalDate", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: LocalDate) {
        val string = value.format(formatter)
        encoder.encodeString(string)
    }

    override fun deserialize(decoder: Decoder): LocalDate {
        val string = decoder.decodeString()
        return LocalDate.parse(string, formatter)
    }
}
@Serializable
data class Youtuber(
    val id: Int,
    var firstName: String,
    var lastName: String,
    @Contextual val dateOfBirth: LocalDate,
    var gender: String,
    @DrawableRes val profilePicture: Int,
    var yearsExperience: Int,
    val youtubeChannels: List<YoutubeChannel>
)

class Functions {
    private val youtubersList: MutableList<Youtuber> = mutableListOf(
        Youtuber(
            1, "Linus", "Sebastian", LocalDate.of(1986, 8, 20), "Male", R.drawable.sebas, 13,
            listOf(
                YoutubeChannel(
                    1, "Linus Tech Tips", "Tech", 1000000, R.drawable.ltt, 1
                ),
                YoutubeChannel(
                    2, "Channel Super Fun", "Tech", 500000, R.drawable.csf, 1
                ),
                YoutubeChannel(
                    19, "Techquickie", "Tech", 1000000, R.drawable.tq, 1
                ),
                YoutubeChannel(
                    20, "Short Circuit", "Tech", 500000, R.drawable.sc, 1
                )
            )
        ),
        Youtuber(
            2, "Andrew", "Rea", LocalDate.of(1986, 9, 2), "Male", R.drawable.andrew, 5,
            listOf(
                YoutubeChannel(
                    3, "Binging with Babish", "Cooking", 2000000, R.drawable.bb, 2
                )
            )
        ),
        Youtuber(
            3, "Christian", "LeBlanc", LocalDate.of(1991, 8, 14), "Male", R.drawable.christian, 7,
            listOf(
                YoutubeChannel(
                    4, "Lost LeBlanc", "Travel", 1500000, R.drawable.ll, 3
                )
            )
        ),
        Youtuber(
            4, "Felix", "Kjellberg", LocalDate.of(1989, 10, 24), "Male", R.drawable.kjelberg, 12,
            listOf(
                YoutubeChannel(
                    5, "PewDiePie", "Gaming", 110000000, R.drawable.pewd1, 4
                ),
                YoutubeChannel(
                    5, "PewDiePie Highlights", "Gaming", 110000000, R.drawable.pewd2, 4
                )
            )
        ),
        Youtuber(
            5, "Anna", "Wintour", LocalDate.of(1949, 11, 3), "Female", R.drawable.wintour, 32,
            listOf(
                YoutubeChannel(
                    6, "Vogue", "Fashion", 5000000, R.drawable.vogue, 5
                )
            )
        ),
        Youtuber(
            6, "Casey", "Neistat", LocalDate.of(1981, 3, 25), "Male", R.drawable.neistat, 14,
            listOf(
                YoutubeChannel(
                    7, "CaseyNeistat", "Vlogs", 8000000, R.drawable.cn, 6
                ),
                YoutubeChannel(
                    8, "368", "Vlogs", 3000000, R.drawable.cn2, 6
                )
            )
        ),
        Youtuber(
            7, "Jenna", "Marbles", LocalDate.of(1986, 9, 15), "Female", R.drawable.marbles, 12,
            listOf(
                YoutubeChannel(
                    9, "JennaMarbles", "Comedy", 15000000, R.drawable.jm, 7
                )
            )
        ),
        Youtuber(
            8, "Mark", "Rober", LocalDate.of(1980, 3, 11), "Male", R.drawable.rob, 9,
            listOf(
                YoutubeChannel(
                    10, "Mark Rober", "Science", 10000000, R.drawable.mr, 8
                )
            )
        ),
        Youtuber(
            9, "David", "Dobrik", LocalDate.of(1996, 7, 23), "Male", R.drawable.david, 6,
            listOf(
                YoutubeChannel(
                    11, "David Dobrik", "Vlogs", 15000000, R.drawable.dd, 9
                )
            )
        ),
        Youtuber(
            10, "Emma", "Chamberlain", LocalDate.of(2001, 5, 22), "Female", R.drawable.emma, 4,
            listOf(
                YoutubeChannel(
                    12, "Emma Chamberlain", "Vlogs", 9000000, R.drawable.ec, 10
                )
            )
        ),
        Youtuber(
            11, "Jimmy", "Donaldson", LocalDate.of(1998, 5, 7), "Male", R.drawable.jimmy, 9,
            listOf(
                YoutubeChannel(
                    13, "MrBeast", "Charity", 40000000, R.drawable.beast, 11
                ),
                YoutubeChannel(
                    14, "Beast Reacts", "Reaction", 5000000, R.drawable.reacts, 11
                ),
                YoutubeChannel(
                    13, "MrBeast Gaming", "Gaming", 40000000, R.drawable.gaming, 11
                ),
                YoutubeChannel(
                    14, "MrBeast Shorts", "Reaction", 5000000, R.drawable.shorts, 11
                )
            )
        ),
        Youtuber(
            12, "Safiya", "Nygaard", LocalDate.of(1992, 7, 16), "Female", R.drawable.saf, 6,
            listOf(
                YoutubeChannel(
                    15, "Safiya Nygaard", "Fashion", 7000000, R.drawable.sn, 12
                )
            )
        ),
        Youtuber(
            13, "Philip", "DeFranco", LocalDate.of(1985, 12, 1), "Male", R.drawable.phil, 14,
            listOf(
                YoutubeChannel(
                    16, "Philip DeFranco", "News", 7000000, R.drawable.pd, 13
                )
            )
        ),
        Youtuber(
            14, "Nikkie", "Tutorials", LocalDate.of(1994, 3, 2), "Female", R.drawable.nikkie, 12,
            listOf(
                YoutubeChannel(
                    17, "NikkieTutorials", "Beauty", 14000000, R.drawable.nt, 14
                )
            )
        ),
        Youtuber(
            15, "Dan", "Howell", LocalDate.of(1991, 6, 11), "Male", R.drawable.howell, 12,
            listOf(
                YoutubeChannel(
                    18, "Daniel Howell", "Vlogs", 6000000, R.drawable.dh, 15
                )
            )
        )
    )

    fun getYoutubers(): List<Youtuber> {
        return youtubersList.toList()
    }

    fun updateExperience(id: Int, newExperience: Int) {
        val youtuber = youtubersList.find { it.id == id }
        youtuber?.let {
            youtuber.yearsExperience = newExperience
        }
    }
}
