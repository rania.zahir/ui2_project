package com.example.groeiproject.network

import com.example.groeiproject.model.YoutubeChannel
import com.example.groeiproject.model.Youtuber
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface YoutubeApiService {
    @GET("youtubechannel")
    suspend fun getYoutubechannelList(): Response<List<YoutubeChannel>>

    @GET("youtubechannel/{id}")
    suspend fun getYoutubechannelById(
        @Path("id") id: Int
    ): Response<YoutubeChannel>

    @GET("youtuber")
    suspend fun getYoutuberList(): Response<List<Youtuber>>

    @GET("youtuber/{id}")
    suspend fun getYoutuberById(
        @Path("id") id: Int
    ): Response<Youtuber>

    @POST("youtuber")
    suspend fun addYoutuber(
        @Body youtuber: Youtuber
    ): Response<Youtuber>

    @DELETE("youtuber/{id}")
    suspend fun deleteYoutuber(
        @Path("id") id: String
    ): Response<Unit>

    @PUT("youtuber/{id}")
    suspend fun updateYoutuber(
        @Path("id") id: Int,
        @Body youtuber: Youtuber
    ): Response<Youtuber>

}