package com.example.groeiproject.ui.screen

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.groeiproject.R
import com.example.groeiproject.model.Functions
import com.example.groeiproject.model.Youtuber
import com.example.groeiproject.ui.theme.GroeiProjectTheme
import java.time.LocalDate

class CreateScreen : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GroeiProjectTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    YoutuberApp()
                }
            }
        }
    }

    @Composable
    fun YoutuberApp() {
        val functions = remember { Functions() }
        var firstName by remember { mutableStateOf("") }
        var lastName by remember { mutableStateOf("") }
        var dateOfBirth by remember { mutableStateOf("") }
        var gender by remember { mutableStateOf("") }
        var profilePicture by remember { mutableStateOf("") }
        var yearsExperience by remember { mutableStateOf("") }

        val onAddClick: () -> Unit = {
            if (firstName.isNotBlank() && lastName.isNotBlank() && dateOfBirth.isNotBlank() && gender.isNotBlank() && yearsExperience.isNotBlank()) {
                val newYoutuber = Youtuber(
                    id = functions.getYoutubers().size + 1,
                    firstName = firstName,
                    lastName = lastName,
                    dateOfBirth = LocalDate.parse(dateOfBirth),
                    gender = gender,
                    profilePicture = R.drawable.uk, // Replace with actual drawable resource
                    yearsExperience = yearsExperience.toInt(),
                    youtubeChannels = emptyList()
                )
                functions.getYoutubers().toMutableList().add(newYoutuber)
            }
        }

        Column(modifier = Modifier.padding(16.dp)) {
            OutlinedTextField(
                value = firstName,
                onValueChange = { firstName = it },
                label = { Text("First Name") },
                modifier = Modifier.fillMaxWidth().padding(vertical = 4.dp)
            )
            OutlinedTextField(
                value = lastName,
                onValueChange = { lastName = it },
                label = { Text("Last Name") },
                modifier = Modifier.fillMaxWidth().padding(vertical = 4.dp)
            )
            OutlinedTextField(
                value = dateOfBirth,
                onValueChange = { dateOfBirth = it },
                label = { Text("Date of Birth (YYYY-MM-DD)") },
                modifier = Modifier.fillMaxWidth().padding(vertical = 4.dp),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
            )
            OutlinedTextField(
                value = gender,
                onValueChange = { gender = it },
                label = { Text("Gender") },
                modifier = Modifier.fillMaxWidth().padding(vertical = 4.dp)
            )
            OutlinedTextField(
                value = profilePicture,
                onValueChange = { profilePicture = it },
                label = { Text("Profile Picture Resource ID") },
                modifier = Modifier.fillMaxWidth().padding(vertical = 4.dp)
            )
            OutlinedTextField(
                value = yearsExperience,
                onValueChange = { yearsExperience = it },
                label = { Text("Years of Experience") },
                modifier = Modifier.fillMaxWidth().padding(vertical = 4.dp),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
            )
            Button(
                onClick = onAddClick,
                modifier = Modifier.align(Alignment.End).padding(vertical = 8.dp)
            ) {
                Text("Add Youtuber")
            }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun YoutuberAppPreview() {
        GroeiProjectTheme {
            YoutuberApp()
        }
    }
}
