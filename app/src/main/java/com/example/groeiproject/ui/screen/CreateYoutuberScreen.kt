package com.example.groeiproject.ui.screen

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.groeiproject.R
import com.example.groeiproject.model.Youtuber
import java.time.LocalDate

@Composable
fun CreateYoutuberScreen(navController: NavController, addYoutuber: (Youtuber) -> Unit) {
    var firstName by remember { mutableStateOf("") }
    var lastName by remember { mutableStateOf("") }
    var gender by remember { mutableStateOf("") }
    var experience by remember { mutableStateOf("") }
    var dateOfBirth by remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TextField(
            value = firstName,
            onValueChange = { firstName = it },
            label = { Text("First Name") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(8.dp))
        TextField(
            value = lastName,
            onValueChange = { lastName = it },
            label = { Text("Last Name") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(8.dp))
        TextField(
            value = gender,
            onValueChange = { gender = it },
            label = { Text("Gender") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(8.dp))
        TextField(
            value = experience,
            onValueChange = { experience = it },
            label = { Text("Years Experience") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(8.dp))
        TextField(
            value = dateOfBirth,
            onValueChange = { dateOfBirth = it },
            label = { Text("Date of Birth (YYYY-MM-DD)") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = {
                val newYoutuber = Youtuber(
                    id = (1..1000).random(), // Generate a random ID
                    firstName = firstName,
                    lastName = lastName,
                    dateOfBirth = LocalDate.parse(dateOfBirth),
                    gender = gender,
                    profilePicture = R.drawable.uk,
                    yearsExperience = experience.toIntOrNull() ?: 0,
                    youtubeChannels = listOf()
                )
                addYoutuber(newYoutuber)
                navController.popBackStack() // Navigate back to the list screen
            },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(text = "Create Youtuber")
        }
    }
}