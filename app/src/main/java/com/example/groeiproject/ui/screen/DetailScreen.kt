package com.example.groeiproject.ui.screen

import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.groeiproject.R
import com.example.groeiproject.model.YoutubeChannel
import com.example.groeiproject.model.Youtuber
import com.example.groeiproject.ui.theme.GroeiProjectTheme

@Composable
fun YoutuberItem(youtuber: Youtuber) {
    // Construct the image URL
    val imageUrl = "http://10.0.2.2:3000/youtubers${youtuber.profilePicture}"

    // Log the URL to verify it's correct
    Log.d("ImageURL", "Youtuber image URL: $imageUrl")

    Column {
        // Use Coil or similar library to load images from URL
        Image(
            painter = rememberImagePainter(data = "http://10.0.2.2:3000/youtubers${youtuber.profilePicture}"),
            contentDescription = "",
            modifier = Modifier.size(128.dp),
            contentScale = ContentScale.Crop
        )
        Text(text = "${youtuber.firstName} ${youtuber.lastName}")
    }
}

@Composable
fun YoutubeChannelItem(channel: YoutubeChannel) {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .padding(8.dp)
            .height(100.dp)
            .width(100.dp)
    ) {
        Column(
            modifier = Modifier.padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Image(
                painter = rememberImagePainter(data = "http://10.0.2.2:3000/youtubers${channel.profilePicture}"),
                contentDescription = "Channel Profile Picture",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(40.dp)
                    .aspectRatio(1f)
            )
            Text(
                text = channel.name,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(top = 4.dp)
            )
            Text(
                text = channel.category,
                style = MaterialTheme.typography.bodySmall
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailScreen(
    youtuber: Youtuber,
    youtubers: List<Youtuber>,
    currentIndex: Int,
    onExperienceUpdated: (Int, Int) -> Unit,
    onDeleteYoutuber: (Int) -> Unit,
    onNavigateToYoutuber: (Int) -> Unit,
    onNavigateBack: () -> Unit,
    onCreateYoutuber: () -> Unit,
    onNavigateToSettings: () -> Unit,
    languageViewModel: LanguageViewModel,
) {
    var newExperience by remember { mutableStateOf(youtuber.yearsExperience.toString()) }
    var updatedExperience by remember { mutableStateOf(youtuber.yearsExperience) }

    LaunchedEffect(youtuber) {
        newExperience = youtuber.yearsExperience.toString()
        updatedExperience = youtuber.yearsExperience
    }

    GroeiProjectTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                TopAppBar(
                    title = { },
                    navigationIcon = {
                        IconButton(onClick = onNavigateBack) {
                            Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Back")
                        }
                    },
                    actions = {
                        IconButton(
                            onClick = {
                                val prevIndex = if (currentIndex > 0) currentIndex - 1 else youtubers.size - 1
                                onNavigateToYoutuber(youtubers[prevIndex].id)
                            }
                        ) {
                            Text(text = "Prev")
                        }
                        Spacer(modifier = Modifier.width(8.dp))
                        IconButton(
                            onClick = {
                                val nextIndex = (currentIndex + 1) % youtubers.size
                                onNavigateToYoutuber(youtubers[nextIndex].id)
                            }
                        ) {
                            Text(text = "Next")
                        }
                        IconButton(onClick = onNavigateToSettings) {
                            Text(text = "Settings")
                        }
                    }
                )

                // Use YoutuberItem here to load the image from the JSON server
                YoutuberItem(youtuber = youtuber)

                Column(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "${youtuber.firstName} ${youtuber.lastName}",
                        modifier = Modifier.padding(bottom = 8.dp)
                    )
                    Text(
                        text = stringResource(id = R.string.gender) + ": ${youtuber.gender}",
                        modifier = Modifier.padding(bottom = 8.dp)
                    )
                    Text(
                        text = "date of birth: ${youtuber.dateOfBirth}",
                        modifier = Modifier.padding(bottom = 8.dp)
                    )
                    Text(
                        text = "years experience: ${youtuber.yearsExperience}",
                        modifier = Modifier.padding(bottom = 4.dp)
                    )

                    // Youtube Channels Section
                    val lazyListState = rememberLazyListState()
                    LazyRow(
                        state = lazyListState,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 16.dp)
                    ) {
                        items(youtuber.youtubeChannels) { channel ->
                            YoutubeChannelItem(channel = channel)
                        }
                    }

                    // Experience update section
                    TextField(
                        value = newExperience,
                        onValueChange = { newExperience = it },
                        label = { Text(text = "years experience") },
                        modifier = Modifier.fillMaxWidth()
                    )
                    Button(
                        onClick = {
                            updatedExperience = newExperience.toIntOrNull() ?: youtuber.yearsExperience
                            onExperienceUpdated(youtuber.id, updatedExperience)
                        },
                        modifier = Modifier.fillMaxWidth(),
                        contentPadding = PaddingValues(4.dp),
                    ) {
                        Text(text = "update experience")
                    }

                    Spacer(modifier = Modifier.height(8.dp))
                    Button(
                        onClick = { onDeleteYoutuber(youtuber.id) },
                        modifier = Modifier.fillMaxWidth(),
                        colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.error),
                        contentPadding = PaddingValues(4.dp),
                    ) {
                        Text(text = "delete")
                    }

                    Spacer(modifier = Modifier.height(8.dp))
                    Button(
                        onClick = onCreateYoutuber,
                        modifier = Modifier.fillMaxWidth(),
                        contentPadding = PaddingValues(4.dp),
                    ) {
                        Text(text = "Create New Youtuber")
                    }
                }
            }
        }
    }
}