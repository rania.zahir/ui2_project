package com.example.groeiproject.ui.screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LanguageViewModel : ViewModel() {
    private val _selectedLanguage = MutableLiveData("nl")
    val selectedLanguage: LiveData<String> get() = _selectedLanguage

    fun setLanguage(language: String) {
        _selectedLanguage.value = language
    }

}