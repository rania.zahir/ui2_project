package com.example.groeiproject.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.groeiproject.R
import com.example.groeiproject.model.Youtuber

@Composable
fun YoutuberListScreen(youtubers: List<Youtuber>, navController: NavController) {
    Column {
        TopBar()
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            items(youtubers) { youtuber ->
                YoutuberListItem(youtuber, navController)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar() {
    TopAppBar(
        title = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberImagePainter(data = "http://10.0.2.2:3000/images/logo.png"),
                    contentDescription = "App Logo",
                    modifier = Modifier
                        .size(40.dp)
                        .padding(end = 8.dp)
                )
                Text(text = "Youtuber List")
            }
        },
        modifier = Modifier.height(64.dp),
        actions = {}
    )
}

@Composable
fun YoutuberListItem(youtuber: Youtuber, navController: NavController) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            // Use YoutuberItem here to load the image from the JSON server
            YoutuberItem(youtuber = youtuber)

            Spacer(modifier = Modifier.width(16.dp))
            Column(modifier = Modifier.weight(1f)) {
                Text(
                    text = "${youtuber.firstName} ${youtuber.lastName}",
                    style = MaterialTheme.typography.bodyMedium
                )
                Text(
                    text = "${youtuber.yearsExperience} years experience",
                    style = MaterialTheme.typography.bodyMedium
                )
            }
            Button(onClick = { navController.navigate("detail/${youtuber.id}") }) {
                Text(text = "Details")
            }
        }
    }
}