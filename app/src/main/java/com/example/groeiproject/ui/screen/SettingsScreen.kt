package com.example.groeiproject.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.groeiproject.R
import com.example.groeiproject.utils.SharedPreferencesHelper


@Composable
fun SettingsScreen(
    sharedPreferencesHelper: SharedPreferencesHelper,
    languageViewModel: LanguageViewModel = viewModel()
) {
    // Remember the current theme preference
    val themePreference = remember { mutableStateOf(sharedPreferencesHelper.getThemePreference()) }
    val selectedLanguage by languageViewModel.selectedLanguage.observeAsState("nl")

    // Detect language changes and load corresponding strings
    val settings = when (selectedLanguage) {
        "en" -> arrayOf(R.string.theme_en, R.string.dark_mode_en)
        "nl" -> arrayOf(R.string.theme, R.string.dark_mode)
        else -> emptyArray()
    }

    // Column layout for the settings
    Column(modifier = Modifier.padding(16.dp)) {
        Text(text = stringResource(id = settings[0]))

        // Dark Mode Toggle
        OutlinedTextField(
            value = themePreference.value,
            placeholder = { Text(text = stringResource(id = settings[1])) },
            onValueChange = { newValue ->
                themePreference.value = newValue

                // Update the theme preference based on user input
                sharedPreferencesHelper.setThemePreference(newValue)
            },
            label = { Text(text = stringResource(id = settings[0])) }
        )
    }
}