package com.example.groeiproject.ui.screen

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.groeiproject.data.YoutubeRepository
import com.example.groeiproject.model.YoutubeChannel
import com.example.groeiproject.model.Youtuber
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

sealed interface YoutubeUiState {
    object Loading : YoutubeUiState
    object Error : YoutubeUiState
    data class Success(
        val youtubechannel: List<YoutubeChannel>,
        val youtuber: List<Youtuber>? = null,
        val userMessage: String? = null
    ) : YoutubeUiState
}


@HiltViewModel
class YoutubeViewModel @Inject constructor(private val youtubeRepository: YoutubeRepository) : ViewModel() {
    var youtubeUiState: YoutubeUiState by mutableStateOf(YoutubeUiState.Loading)
        private set

    init {
        getYoutubechannel()
        getAllYoutuber()
    }


    private val _youtubechannels = MutableLiveData<List<YoutubeChannel>>(emptyList())
    val youtubechannels: LiveData<List<YoutubeChannel>> get() = _youtubechannels

    private val _youtuber = MutableLiveData<List<Youtuber>?>()
    val youtuber: LiveData<List<Youtuber>?> get() = _youtuber


    fun getYoutubechannel() {
        viewModelScope.launch {
            try {
                val listResult: List<YoutubeChannel> = youtubeRepository.getChannels()
                Log.d("getYoutubechannel", "${listResult.size}")
                if (listResult.isEmpty()) {
                    youtubeUiState = YoutubeUiState.Error
                } else {
                    _youtubechannels.value = listResult
                    youtubeUiState = YoutubeUiState.Success(youtubechannel = listResult)
                }
            } catch (e: Exception) {
                youtubeUiState = YoutubeUiState.Error
                Log.e("getYoutubechannel", "Error fetching ytchannels", e)
            }
        }
    }

    fun getAllYoutuber() {
        viewModelScope.launch {
            try {
                val youtuberList = youtubeRepository.getYoutuber()
                _youtuber.value = youtuberList
            } catch (e: Exception) {
                Log.e("getAllYoutuber", "Error fetching youtubers", e)
            }
        }
    }

    fun getYoutuberByYoutubechannel(youtubechannelId: Int) {
        viewModelScope.launch {
            try {
                val listResult: List<Youtuber>? =
                    youtubeRepository.getYoutuberByYoutubechannelId(youtubechannelId)
                if (listResult != null) {
                    Log.d("getYoutuber", "${listResult.size}")
                    youtubeUiState =
                        YoutubeUiState.Success(youtubechannel = emptyList(), youtuber = listResult)
                    _youtuber.value = listResult
                } else {
                    youtubeUiState = YoutubeUiState.Error
                    Log.e("getYoutuber", "No youtubers found")
                    _youtuber.value = emptyList()
                }
            } catch (e: Exception) {
                youtubeUiState = YoutubeUiState.Error
                Log.e("getYoutuber", "Error fetching youtubers", e)
                _youtuber.value = emptyList()
            }
        }
    }

    fun addYoutuber(youtuber: Youtuber) {
        viewModelScope.launch {
            try {
                val newYoutuber = youtubeRepository.addYoutuber(youtuber)
                successMessage("New youtuber added id=${newYoutuber?.id}")
                getAllYoutuber()
            } catch (e: Exception) {
                youtubeUiState = YoutubeUiState.Error
                Log.e("addYoutuber", "Error adding youtuber", e)
            }
        }
    }

    fun updateYoutuber(youtuber: Youtuber) {
        viewModelScope.launch {
            try {
                val updatedYoutuber = youtubeRepository.updateYoutuber(youtuber)
                successMessage("youtuber updated id=${updatedYoutuber?.id}")
                getAllYoutuber()
            } catch (e: Exception) {
                youtubeUiState = YoutubeUiState.Error
                Log.e("updateYoutuber", "Error updating youtube", e)
            }
        }
    }

    fun getNextYoutuberId(allYoutuber: List<Youtuber>): Int {
        val uniqueIds = allYoutuber.map { it.id }.distinct()
        val maxId = uniqueIds.maxOrNull() ?: 0
        return maxId + 1
    }


    private fun successMessage(message: String) {
        if (youtubeUiState is YoutubeUiState.Success) {
            val currentState = youtubeUiState as YoutubeUiState.Success
            youtubeUiState =
                YoutubeUiState.Success(
                    youtubechannel = currentState.youtubechannel,
                    userMessage = message
                )
        }
    }

    fun deleteYoutuberById(youtuberId: Int) {
        viewModelScope.launch {
            try {
                val deleted = youtubeRepository.deleteYoutuber(youtuberId.toString())
                if (deleted) successMessage("youtuber deleted id=$youtuberId")
                getAllYoutuber()
            } catch (e: Exception) {
                youtubeUiState = YoutubeUiState.Error
                Log.e("deleteYoutuberById", "Error deleting youtuber", e)
            }
        }
    }

    fun getNumberOfYoutubechannel(): Int {
        return youtubechannels.value?.size ?: 0
    }

    fun getYoutuberById(youtuberId: Int): Youtuber? {
        return youtuber.value?.find { it.id == youtuberId }
    }

    fun getYoutubechannelById(youtubechannelId: Int): YoutubeChannel? {
        return youtubechannels.value?.find { it.id == youtubechannelId }
    }

}