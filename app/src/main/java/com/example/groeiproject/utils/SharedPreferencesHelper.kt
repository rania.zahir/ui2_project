package com.example.groeiproject.utils

import android.content.Context
import android.content.SharedPreferences
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.distinctUntilChanged

class SharedPreferencesHelper(context : Context) {
    private val sharedPreferences = context.getSharedPreferences("youtuber_ytchannel", Context.MODE_PRIVATE)

    val themePreferenceFlow: Flow<String> = callbackFlow {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (key == "theme_preference") {
                trySend(getThemePreference())
            }
        }
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
        awaitClose { sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener) }
    }.distinctUntilChanged()

    fun getThemePreference(): String {
        return sharedPreferences.getString("theme_preference", "light") ?: "light"
    }

    fun setThemePreference(theme: String) {
        with(sharedPreferences.edit()) {
            putString("theme_preference", theme)
            apply()
        }
    }

    fun getFontPreference(): String {
        return sharedPreferences.getString("font_preference", "default") ?: "default"
    }

    fun setFontPreference(font: String) {
        with(sharedPreferences.edit()) {
            putString("font_preference", font)
            apply()
        }
    }

    val fontPreferenceFlow: Flow<String> = callbackFlow {
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (key == "font_preference") {
                trySend(getFontPreference())
            }
        }
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
        awaitClose { sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener) }
    }.distinctUntilChanged()
}